#include "XBee.h"
#include "Arduino.h"

//declare type and instance of the object for the XBee API and response message
XBee xbee = XBee();
XBeeResponse response = XBeeResponse();
// create reusable response objects for responses we expect to handle
Rx16Response rx16 = Rx16Response();
TxStatusResponse txStatus = TxStatusResponse();

uint8_t option = 0;
uint8_t data = 0;

//converts ASCII to int
int ASCIItoInt(char c)
{
  if ((c >= '0') && (c <= '9'))
    return c - 0x30; // Minus 0x30
  else if ((c >= 'A') && (c <= 'F'))
    return c - 0x37; // Minus 0x41 plus 0x0A
  else if ((c >= 'a') && (c <= 'f'))
    return c - 0x57; // Minus 0x61 plus 0x0A
  else
    return -1;
}

//read cmd message and store in array ar[]
void processPayload(uint8_t *DATA, int ar[]) {

  int value = ASCIItoInt(DATA[1]) * 100; // Convert next three
  value += ASCIItoInt(DATA[2]) * 10;     // chars to a 3-digit
  value += ASCIItoInt(DATA[3]);
  ar[0] = DATA[0];
  ar[1] = value;
}


// continuously reads packets, looking for RX16 or RX16, and save pakcet messages in cmd[]
void rx(int cmd[]) {
  xbee.setSerial(Serial1);
  xbee.readPacket();

  int arr[3];
  
  if (xbee.getResponse().isAvailable()) {
    // got something
    if (xbee.getResponse().getApiId() == RX_16_RESPONSE) {
      // got a rx packet
      xbee.getResponse().getRx16Response(rx16);
      processPayload(rx16.getData(), arr);

      cmd[0]=arr[0];
      cmd[1]=arr[1];

    }else if (xbee.getResponse().getApiId() == TX_STATUS_RESPONSE) {
     xbee.getResponse().getTxStatusResponse(txStatus);
      cmd[0] = 0;
      cmd[1] = 0;
    }else {
      Serial.println("address should be 16-bit address");
    }
     
  }else if (xbee.getResponse().isError()) {
    Serial.println(xbee.getResponse().getErrorCode());
  }
}

//transmit feedback message, where int values are split to high and low byte for transmission
void tx(int *to_send) {
  char c = to_send[0];
  int value = to_send[1];
  int value2 = to_send[2];

  uint8_t high = highByte(value);
  uint8_t low = lowByte(value);
  uint8_t high2 = highByte(value2);
  uint8_t low2 = lowByte(value2);

  // allocate two bytes for to hold a 10-bit analog reading
  uint8_t payload[] = {c, high, low, high2, low2};

  Tx16Request tx = Tx16Request(0x0, payload, sizeof(payload));
  xbee.send(tx);
}

