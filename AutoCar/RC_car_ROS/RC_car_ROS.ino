#include <IMUturn.h>
#include "xbee_transceive.h"

//Motor 1
int pinAIN1 = 8; //Direction
int pinAIN2 = 9; //Direction
int pinPWMA = 6; //Speed
//Motor 2
int pinBIN1 = 17; //Direction
int pinBIN2 = 15; //Direction
int pinPWMB = 5; //Speed
//Standby
int pinSTBY = 10;

const float diskcount = 20.00; //one encoder disk has 20 slots
const float diam = 66.10; //diameter of the wheel in mm

//virtual external interrupt pins, defined by EnableInterrupt library
int motorcount1 = 14; //external interrupt for the slot disk on the left wheel
int motorcount2 = 16; //external interrupt for the slot disk on the right wheel

static int IMUturn::_counter1; //declare global variable to use interrupt within the IMUturn class
static int IMUturn::_counter2; //declare global variable to use interrupt within the IMUturn class
int Speed = 220; //PWM controlled analog write, between 0 and 255

//declare the drive object
IMUturn drive(motorcount1, motorcount2, diskcount, diam, pinAIN1, pinAIN2, pinPWMA, pinBIN1, pinBIN2, pinPWMB, pinSTBY);

void setup() {
  drive.IMU_init(); //initialize IMU MPU6050 and calibrate
  delay(5000); //wait 5s for IMU calibration
  Serial1.begin(9600); //begin XBee serial1 communication
  Serial.begin(9600); //begin serial communication for printing and debugging
}

char c; //a char for direction control
int rx_arr[3]; //declare array to store received command

void loop() {

  rx(rx_arr);//read data from XBee (Serial1) using rx() from "xbee_transceive.h", and store in rx_arr

  if (xbee.getResponse().isAvailable()) {

    c = rx_arr[0];
    
    //switch based on direction command char and call corresponding control functions
    switch (c)
    {
      case 'w':
        forward(rx_arr[1]);
        break;
      case 's':
        backward(rx_arr[1]);
        break;
      case 'a':
        left(rx_arr[1]);
        break;
      case 'd':
        right(rx_arr[1]);
        break;
      default:
        break;
    }
  }
}

int tx_arr[3];//feedback array to be transmitted later, including cmd direction, distance travelled, and current yaw
int fback[3];
float offset = 10.0;//to compensate turning errors
unsigned long t0, t1, td;//timer heuristics to detect glitches of the speed encoders
unsigned long t_th=400;


void forward(float value) {
  t0 = millis();
  //class methods that drives forward at certain speed for certain distance in cm
  //value is in cm, which needs to converted to encoder tick counts by the CMtoCounts class method
  //speed is analog write in the range of [0, 255]
  //fback will be written with the interpolated yaw angle 
  drive.goForward(drive.CMtoCounts(value), Speed, fback);
  t1 = millis();
  td = t1 - t0;

  tx_arr[0] = 'w';
  //if execution time less the 400ms, the speed encoders then glitched, setting fback to 0 distance travelled 
  if(td > t_th){
    tx_arr[1] = (int) value;
  }else{
    tx_arr[1] = (int) 0;
  }
  //otherwise we assume that the specified distance has been executed
  tx_arr[2] = -(int) fback[0];
  tx(tx_arr);
  Serial.print("Went forward for ");
  Serial.print(value);
  Serial.print("cm with agv yaw: ");
  Serial.println(tx_arr[2]);

}

void backward(float value) {
  t0 = millis();
  //class methods that drives backward at certain speed for certain distance in cm
  //value is in cm, which needs to converted to encoder tick counts by the CMtoCounts class method
  //speed is analog write in the range of [0, 255]
  //fback will be written with the interpolated yaw angle 
  drive.goBackward(drive.CMtoCounts(value), Speed, fback);
  t1 = millis();
  td = t1 - t0;
  tx_arr[0] = 's';
  //if execution time less the 400ms, most likely the speed encoders have glitched, setting fback to 0 distance travelled 
  if(td > t_th){
    tx_arr[1] = (int) value;
  }else{
    tx_arr[1] = (int) 0;
  }
  //otherwise we assume that the specified distance has been executed
  tx_arr[2] = -(int) fback[0];
  tx(tx_arr);
  Serial.print("Went backward for ");
  Serial.print(value);
  Serial.print("cm with agv yaw: ");
  Serial.println(tx_arr[2]);
}

void left(float value) {
  //left needs to be converted to negative degrees before applying the class method
  //offset compensates for turning errors due to inertia
  float deg;
  if(value<10){
    deg = -value;
  }else{
    deg = (float) - (1.0 * value - offset);
  }

  //class method that turns to target deg at specified speed, and gives feedback of the current yaw after turning
  drive.turn(deg, Speed, fback);
  tx_arr[0] = 'a';
  tx_arr[1] = (int) value;
  tx_arr[2] = -(int) fback[0];
  tx(tx_arr);
  Serial.print("Current orientation: ");
  Serial.print(tx_arr[2]);
}

void right(float value) {
  //offset compensates for turning errors due to inertia
  float deg;
  if(value<10){
    deg = value;
  }else{
    deg = (float) value - offset;    
  }

  //class method that turns to target deg at specified speed, and gives feedback of the current yaw after turning
  drive.turn(deg, Speed, fback);
  tx_arr[0] = 'd';
  tx_arr[1] = (int) value;
  tx_arr[2] = -(int) fback[0];
  tx(tx_arr);
  Serial.print("Current orientation: ");
  Serial.print(tx_arr[2]);
}

