#include <IMUturn.h>

//Motor 1
int pinAIN1 = 8; //Direction
int pinAIN2 = 9; //Direction
int pinPWMA = 6; //Speed
//Motor 2
int pinBIN1 = 17; //Direction
int pinBIN2 = 15; //Direction
int pinPWMB = 5; //Speed
//Standby
int pinSTBY = 10;

const float diskcount = 20.00; //one encoder disk has 20 slots
const float diam = 66.10; //diameter of the wheel in mm
//virtual external interrupt pins, defined by EnableInterrupt library
int motorcount1 = 14; //external interrupt for the slot disk on the left wheel
int motorcount2 = 16; //external interrupt for the slot disk on the right wheel

static int IMUturn::_counter1; //declare global variable to use interrupt within the IMUturn class
static int IMUturn::_counter2; //declare global variable to use interrupt within the IMUturn class
int Speed = 250; //PWM controlled analog write, between 0 and 255
int value; //command value sent via Xbee

//declare the drive object
IMUturn drive(motorcount1, motorcount2, diskcount, diam, pinAIN1, pinAIN2, pinPWMA, pinBIN1, pinBIN2, pinPWMB, pinSTBY);

void setup() {
  drive.IMU_init(); //initialize IMU MPU6050 and calibrate
  delay(5000); //wait 5s for IMU calibration
  Serial1.begin(9600); //begin XBee serial1 communication
  Serial.begin(9600); //begin serial communication for printing and debugging
}

char c;

void loop() {
  //read XBee data when available and switch base on directional command
  if (Serial1.available())
  {
    char c = Serial1.read();
    Serial.println(c);
    switch (c)
    {
      case 'w':
        forward();
        break;
      case 's':
        backward();
        break;
      case 'a':
        left();
        break;
      case 'd':
        right();
        break;
    }
  }
}

//convert chars to (upto) 3 digit int
int processPackets() {
  while (Serial1.available() < 3);
  int val = ASCIItoInt(Serial1.read()) * 100; // Convert next three
  val += ASCIItoInt(Serial1.read()) * 10;     // chars to a 3-digit
  val += ASCIItoInt(Serial1.read());

  return val;
}

int fback[3];

void forward() {
  value = processPackets();
  //class methods that drives forward at certain speed for certain distance in cm
  //value is in cm, which needs to converted to encoder tick counts by the CMtoCounts class method
  //speed is analog write in the range of [0, 255]
  //fback will be written with the interpolated yaw angle
  drive.goForward(drive.CMtoCounts(value), Speed, fback);
  Serial1.println("Went forward for ");
  Serial1.println(value);
  Serial1.println("cm with agv yaw: ");
  Serial1.println(fback[0]);
}

void backward() {
  value = processPackets();
  //class methods that drives backward at certain speed for certain distance in cm
  //value is in cm, which needs to converted to encoder tick counts by the CMtoCounts class method
  //speed is analog write in the range of [0, 255]
  //fback will be written with the interpolated yaw angle 
  drive.goBackward(drive.CMtoCounts(value), Speed, fback);
  Serial1.println("Went backward for ");
  Serial1.println(value);
  Serial1.println("cm with agv yaw: ");
  Serial1.println(fback[0]);
}

void left() {
  value = processPackets();
  float deg = (float) - 1.0 * value;
  //class method that turns to target deg at specified speed, and gives feedback of the current yaw after turning
  drive.turn(deg, Speed, fback);
  Serial1.println("Current orientation: ");
  Serial1.println(fback[0]);
}

void right() {
  value = processPackets();
  //class method that turns to target deg at specified speed, and gives feedback of the current yaw after turning
  drive.turn(value, Speed, fback);
  Serial1.println("Current orientation: ");
  Serial1.println(fback[0]);
}

//convert ASCII to int
int ASCIItoInt(char c)
{
  if ((c >= '0') && (c <= '9'))
    return c - 0x30; // Minus 0x30
  else if ((c >= 'A') && (c <= 'F'))
    return c - 0x37; // Minus 0x41 plus 0x0A
  else if ((c >= 'a') && (c <= 'f'))
    return c - 0x57; // Minus 0x61 plus 0x0A
  else
    return -1;
}
