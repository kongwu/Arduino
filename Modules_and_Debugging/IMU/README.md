The IMU first needs to be calibrated, run the code in MPU6050_calibration on the Fio, and note down the offset values printed in the serial monitor. 
For more info regarding calibration refer to [here](https://42bots.com/tutorials/arduino-script-for-mpu-6050-auto-calibration/)

get_YAW if modified from the MPU6050 library to do current yaw angle estimation. In the IMUturn library `void set()` is changed to `initialize()`, and `void loop()` is changed to `get_YAW()`

You can also visualize the 3D orientation estimation with processing by following [here](https://maker.pro/arduino/tutorial/how-to-interface-arduino-and-the-mpu-6050-sensor)