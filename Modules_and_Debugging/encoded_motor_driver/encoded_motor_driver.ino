#include <EnableInterrupt.h>

//Define the Pins
//Motor 1
int pinAIN1 = 8; //Direction
int pinAIN2 = 9; //Direction
int pinPWMA = 6; //Speed

//Motor 2
int pinBIN1 = 17; //Direction
int pinBIN2 = 15; //Direction
int pinPWMB = 5; //Speed

//Standby
int pinSTBY = 10;


//Constants to help remember the parameters
static boolean turnCW = 0;  //for motorDrive function
static boolean turnCCW = 1; //for motorDrive function
static boolean motor1 = 0;  //for motorDrive, motorStop, motorBrake functions
static boolean motor2 = 1;  //for motorDrive, motorStop, motorBrake functions

const float diskcount = 20.00;
const float diam = 66.10;
volatile int counter1 = 0;
volatile int counter2 = 0;
int motorcount1 = 14;
int motorcount2 = 16;

int CMtoCounts(float cm) {

  int results;
  float circumference = (diam * 3.14) / 10;
  float cm_count = circumference / diskcount;
  float counts = cm / cm_count;
  results = int(counts);
  return results;

}

void goForward(int counts, int speed)
{
  counter1 = 0;
  counter2 = 0;
  
  while (counts > counter1 && counts > counter2) {
    if (counts > counter1) {
      motorDrive(motor1, turnCW, speed);
    } else {
      motorStop(motor1);
    }
    if (counts > counter2) {
      motorDrive(motor2, turnCW, speed);
    } else {
      motorStop(motor2);
    }
  }

  motorsStandby();
  counter1 = 0;
  counter2 = 0;
}

void goBackward(int counts, int speed)
{
  counter1 = 0;
  counter2 = 0;
  
  while (counts > counter1 && counts > counter2) {
    if (counts > counter1) {
      motorDrive(motor1, turnCCW, speed);
    } else {
      motorStop(motor1);
    }
    if (counts > counter2) {
      motorDrive(motor2, turnCCW, speed);
    } else {
      motorStop(motor2);
    }
  }

  motorsStandby();
  counter1 = 0;
  counter2 = 0;
}

void spinLeft(int counts, int speed)
{
  counter1 = 0;
  counter2 = 0;

  while (counts > counter1 && counts > counter2) {
    if (counts > counter1) {
      motorDrive(motor1, turnCCW, speed);
    } else {
      motorStop(motor1);
    }
    if (counts > counter2) {
      motorDrive(motor2, turnCW, speed);
    } else {
      motorStop(motor2);
    }
  }
  motorsStandby();
  counter1 = 0;
  counter2 = 0;

}

void spinRight(int counts, int speed)
{
  counter1 = 0;
  counter2 = 0;

  while (counts > counter1 && counts > counter2) {
    if (counts > counter1) {
      motorDrive(motor1, turnCW, speed);
    } else {
      motorStop(motor1);
    }
    if (counts > counter2) {
      motorDrive(motor2, turnCCW, speed);
    } else {
      motorStop(motor2);
    }
  }

  motorsStandby();
  counter1 = 0;
  counter2 = 0;
}


void ISR_count1() {
  counter1++;
  //  Serial.print("counter1: ");
  //  Serial.println(counter1);

}

void ISR_count2() {
  counter2++;
  //  Serial.print("counter2: ");
  //  Serial.println(counter2);
}

void motorDrive(boolean motorNumber, boolean motorDirection, int motorSpeed)
{
  /*
    This Drives a specified motor, in a specific direction, at a specified speed:
    - motorNumber: motor1 or motor2 ---> Motor 1 or Motor 2
    - motorDirection: turnCW or turnCCW ---> clockwise or counter-clockwise
    - motorSpeed: 0 to 255 ---> 0 = stop / 255 = fast
  */

  boolean pinIn1;  //Relates to AIN1 or BIN1 (depending on the motor number specified)


  //Specify the Direction to turn the motor
  //Clockwise: AIN1/BIN1 = HIGH and AIN2/BIN2 = LOW
  //Counter-Clockwise: AIN1/BIN1 = LOW and AIN2/BIN2 = HIGH
  if (motorDirection == turnCW)
    pinIn1 = HIGH;
  else
    pinIn1 = LOW;

  //Select the motor to turn, and set the direction and the speed
  if (motorNumber == motor1)
  {
    digitalWrite(pinAIN1, pinIn1);
    digitalWrite(pinAIN2, !pinIn1);  //This is the opposite of the AIN1
    analogWrite(pinPWMA, motorSpeed);
  }
  else
  {
    digitalWrite(pinBIN1, pinIn1);
    digitalWrite(pinBIN2, !pinIn1);  //This is the opposite of the BIN1
    analogWrite(pinPWMB, motorSpeed);
  }



  //Finally , make sure STBY is disabled - pull it HIGH
  digitalWrite(pinSTBY, HIGH);

}

void motorBrake(boolean motorNumber)
{
  /*
    This "Short Brake"s the specified motor, by setting speed to zero
  */
  if (motorNumber == motor1)
    analogWrite(pinPWMA, 0);
  else
    analogWrite(pinPWMB, 0);
}


void motorStop(boolean motorNumber)
{
  /*
    This stops the specified motor by setting both IN pins to LOW
  */
  if (motorNumber == motor1) {
    digitalWrite(pinAIN1, LOW);
    digitalWrite(pinAIN2, LOW);
  }
  else
  {
    digitalWrite(pinBIN1, LOW);
    digitalWrite(pinBIN2, LOW);
  }
}

void motorsStop()
{
  /*
    This stops the specified motor by setting both IN pins to LOW
  */
  digitalWrite(pinAIN1, LOW);
  digitalWrite(pinAIN2, LOW);

  digitalWrite(pinBIN1, LOW);
  digitalWrite(pinBIN2, LOW);
}


void motorsStandby()
{
  /*
    This puts the motors into Standby Mode
  */
  digitalWrite(pinSTBY, LOW);
}

void setup() {

  //Set the PIN Modes
  pinMode(pinPWMA, OUTPUT);
  pinMode(pinAIN1, OUTPUT);
  pinMode(pinAIN2, OUTPUT);
  pinMode(pinPWMB, OUTPUT);
  pinMode(pinBIN1, OUTPUT);
  pinMode(pinBIN2, OUTPUT);
  pinMode(pinSTBY, OUTPUT);

  pinMode(motorcount1, INPUT);
  pinMode(motorcount2, INPUT);

  Serial.begin(9600);

  enableInterrupt(motorcount1, ISR_count1, RISING);
  enableInterrupt(motorcount2, ISR_count2, RISING);

}

void loop()
{
  //go forward for 10 cm
  goForward(CMtoCounts(10), 200);

  motorsStandby();
  delay(1000);
  
  //go backward for 10
  goBackward(CMtoCounts(10), 200);

  motorsStandby();
  delay(1000);

  //spin right for 20 slots (not with IMU yet)
  spinRight(20, 200);

  motorsStandby();
  delay(1000);

  //turn left for 5 slots (not with IMU yet)
  spinLeft(5, 200);
  delay(2000);

  motorsStandby();
  delay(1000);
}

