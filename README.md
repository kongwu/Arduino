# AutoCar Motor Control Library

The AutoCar motor control library deploys directional differential speed control on an Sparkfun Fio v3 based robot car, which comes with feedback from speed sensors on each wheel for accurate distance control, and a 6 DOF IMU sensor (accelerometer + gyroscope) for yaw angle estimation to serve the turning control. The AutoCar can be remote controlled by a PC via a pair of XBee S1 modules.

## Getting Started

### Prerequisites

First, install Arduino IDE, follow instructions [here](https://www.arduino.cc/en/Guide/Linux).

In order to program and upload sketches to Sparkfun Fio v3 using Arduino IDE, the Sparkfun supporting libraries and corresponding Arduino Addons needs to be added, follow the [Fio V3 hookup guide](https://learn.sparkfun.com/tutorials/pro-micro--fio-v3-hookup-guide/installing-mac--linux).

The library also has some dependencies over other pre-existing libraries, which are:

* [I2Cdev](https://github.com/jrowberg/i2cdevlib/tree/master/Arduino/I2Cdev), this library is for communicating with the IMU. Just download the ZIP, unzip it, and copy the I2Cdev folder, or git clone it into your Arduino /libraries.

* [MPU6050](https://github.com/jrowberg/i2cdevlib/tree/master/Arduino/MPU6050), this library is used to estimate and read 3-D angle orientation from the IMU sensor.

* [EnableInterrupt](https://github.com/GreyGnome/EnableInterrupt), this library is used to define virtual external interrupt pins to allow more sensory inputs.

* [XBee-Arduino_library](https://github.com/andrewrapp/xbee-arduino), xbee API library for Arduino.

The above libraries can be git cloned, including the submodules,
although note that the I2Cdev and MPU6050 modules, are subdirectories of the [i2cdevlib](https://github.com/jrowberg/i2cdevlib/) library,
which can't be made to a submodule. They can be cloned from this repository for convenience, but hats off to [jrowberg](https://github.com/jrowberg)

You also need to install ROS for a more modularized and higher level control. Install ROS Kinetic by following instructions [here](http://wiki.ros.org/kinetic/Installation)


### Installing

Create Arduino sketchbook directory, and git clone the code, including the submodules of other libraries:

```
mkdir ~/Arduino
git clone --recurse-submodules git@gitlab.com:kongwu/Arduino.git
```

Go to your Arduino IDE -> File -> Preferences, and set the default sketchbook directory to ~/Arduino . And git clone all the other four libraries mentioned above in the prerequisites to ~/Arduino/libraries.

If you don't have git, install by

```
sudo apt-get install git
```

and generate your SSH by following [this](https://docs.gitlab.com/ee/ssh/README.html), and copy the public key to your Gitlab account.


## Wiki

For assembling the circuit, the AutoCar, and debugging. Please refer to the [wiki](https://gitlab.com/kongwu/Arduino/wikis/home) page.

## Controll the AutoCar
There are three things that can be tested with AutoCar: RC controll using the X-CTU software, RC controll using XBee API and ROS, and PID controll to go in a straight line.

### RC controll using the X-CTU software

For running the `RC_car.ino` sketch, the .ino file needs to be uploaded to the board via Arduino IDE. Note that for Sparkfun Fio V3 extra add-ons and libraries need to be installed from [here](https://learn.sparkfun.com/tutorials/pro-micro--fio-v3-hookup-guide#installing-mac--linux).

If there're problems uploading sketch to board, follow instructions [here](https://stackoverflow.com/questions/40951728/avrdude-ser-open-cant-open-device-dev-ttyacm0-device-or-resource-busy). The most common problems are either not granting permission to the serial port, or that the *modemmanager* is conflicting with the serial port, which can be fixed by purging *modemmanager*

```
sudo apt-get purge modemmanager
```

X-CTU software is needed to configure the XBees and set up the communication. Download and install X-CTU [here](https://www.digi.com/resources/documentation/digidocs/90001526/tasks/t_download_and_install_xctu_linux.htm?tocpath=Set%20up%20%20your%20XBee%20devices%7CDownload%20and%20install%20XCTU%7C_____2). 

Getting familiarized with XBee modules and the X-CTU software, please refer to these [instructions](https://learn.sparkfun.com/tutorials/exploring-xbees-and-xctu).

When configuring the XBee modules, put the XBee on the USB dongle, connect to PC. 

Click on the plus sign to add a radio module, i.e., the corresponding USB port of your PC that's connected to the XBee dongle. Once added, click on the second circle icon to add nearby device, 
which in this case the Fio board should be turned on with the other XBee module attached.

Now go to **Configuration Working Mode** of the PC XBee, set the **CH** and **ID** to a certain channel and PAN ID (the same for the second XBee that will be connected to the Fio board). 
Set **DL** to 1, and **MY** to 0, meaning that the destination address, i.e., Fio XBee is 1, and my address for receiving, i.e., the PC XBee, is 0. Don't forget to click on **Write** after making the changes.

Now we go for the Fio XBee. But before we do so, we need to set **AP** of the PC XBee to 2, click on **Write**, such that the API mode is enabled, allowing us to set parameters wireless for the Fio XBee.

For the second XBee, go to **Configuration Working Mode**, set **DL** to 0, i.e., destimation address is 0 (PC XBee), and my address for receiving is **MY** = 1, two of which are the opposite to the PC XBee such that both XBees can send and receive messages from each other. 

Now set **AP** to 0 for both XBee modules, first write for the Fio XBee, then write for the PC XBee, this is to disable the API mode so that message can be written directly in ASCII code in the X-CTU software in Consoles Working Mode.

After configuring both XBees, leave the first XBee on the USB dongle connected to the PC, the seconed connected to the XBee socket of the Fio board. 

Now restart the Fio board, turn on the switch on the board, and turn on the switch of the motors powered by the AA batteries.

We can now go to the **Console Working Mode** in X-CTU, click on the connect button, and add packets to the AutoCar for remote control. 

The commands are in the format of ascii code, a letter followed by three digits. *w* and *s* stands for going forward and backward, followed by 3 digit integer to specify distance in cm, e.g., when *w010* is sent, the AutoCar will drive forward for 10 cm. 
*a* and *d* then stands for turning left and right, followed also by three digit specifying the turning angle (within the range of \[0, 360]), 
i.e., *d120* will command the AutoCar to turn right for 120 degrees. 

Once you added the packets, select one and click send, the AutoCar should execute the command.

### RC controll using XBee API and ROS

First upload the sketch `RC_car_ROS` to the Fio board via Arduino IDE.

Go to X-CTU, and enable API mode for both XBee modules, i.e., set **AP** to 2.

After installing ROS, follow the [tutorial](http://wiki.ros.org/ROS/Tutorials/InstallingandConfiguringROSEnvironment) to setup the catkin workspace.

Now go to the src directory within the workspace, git clone the code from [here](https://gitlab.com/kongwu/autocar) for ROS integration, and build:

```
cd ~/catkin_ws/src
git clone git@gitlab.com:kongwu/autocar.git
cd ~/catkin_ws
catkin_make
```

Before running the scripts, we need to go to `~/catkin_ws/src/autocar/scritps`, open up `receive_cmd.py`, and set the PORT in the header to the port that's connected to the XBee module, e.g., `PORT = '/dev/ttyUSB0'`.

Now start ROS core, turn on the switches on the AutoCar and wait for 5s for calibration, plug in the XBee dongle to the PC, and run the scritps for publishing and feedback of the commands
(note that each line needs to new terminal to execute):
```
roscore
rosrun autocar publish_cmd.py
rosrun autocar receive_cmd.py
```

If no scripts found, then they're not built due to permission issues, give permission to the two scripts and build again:

```
roscd autocar/scripts
chmod +x publish_cmd.py
chmod +x receive_cmd.py
cd ~/catkin_ws
catkin_make
```

Go to the terminal that runs `publish_cmd.py`, and give command such as 'w010', 'a090', etc., i.e., the same as in the commands sent via the X-CTU software.

Real-time plotting of the trajectory can also be enabled by:

```
rosrun autocar plot.py
```


### PID control driving a straight line
#### PID simulaor
Before implementing PID control on the hardware, we first test it in simulaion. Where ROS messages have been simulated to hand-tune the parameters before implementing on hardware.

The demo can be run by (note that each line needs to be executed in a new terminal):

```
roscore
rosrun autocar straight_line_PID.py
rosrun autocar simulae.py
```

For changing stepsize and tunning parameters of the PID controller, go to the header of straight_line_PID.py

#### PID control on the AutoCar
Using the same sketch `RC_car_ROS` on the Fio board, put the AutoCar in the desired direction of heading, turn on the motor switch, then the board
switch, plug in the XBee dongle to PC, and change the port accordingly in '.../catkin_ws/src/autocar/scripts/start_PID.py', and open
the terminal(each line needs a new terminal to execute):

```
roscore
rosrun autocar autocar_PID.py
rosrun autocar start_PID.py
```

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tips to anyone whose code was used
* This work is supported by [BCS lab](http://www.bcs.tu-darmstadt.de/biocomm/welcome/index.en.jsp) in TU Darmstadt


