#ifndef IMUTURN_H
#define IMUTURN_H

#include "Arduino.h"
#include "motorCtrl.h"



class IMUturn:public motorCtrl
{

  public:
    IMUturn(int motorCount1, int motorCount2, float diskcount, float diam, int pinAIN1, int pinAIN2, int pinPWMA, int pinBIN1, int pinBIN2, int pinPWMB, int pinSTBY);
    virtual float theta_mapping(float theta_0, float Deg);
    virtual void IMU_init();
    virtual float measureYAW();
    virtual float avg_YAW();
    volatile float YAW;
    int CMtoCounts(float cm);    
    static void ISR_count1();
    static void ISR_count2();
    virtual void turn(float Deg, int vol, int fb[]);
    virtual void goForward(int counts, int speed, int fb[]);
    virtual void goBackward(int counts, int speed, int fb[]);


  private:
    volatile int var;//variable for the switch case
    static const boolean turnCW = 0;//clockwise->forward
    static const boolean turnCCW = 1;//counter-clockwise->backward
    static const boolean motor1 = 0;//left motor
    static const boolean motor2 = 1;//right motor
    int _motorCount1;//external interrupt pin for the speed sensor that counts the left slot disk
    int _motorCount2;//external interrupt pin for the speed sensor that counts the right slot disk
    float _diskcount;//number of slots for going one round, which is 20 for this slot disk
    float _diam;//diameter of the wheel
    static int _counter1;//counter for number of slots passed for the left wheel
    static int _counter2;//counter for number of slots passed for the right wheel

};


#endif