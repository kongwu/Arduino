#include "IMUturn.h"
#include "IMU.h"
//below is to avoid conflict with arduino defined external interrupt pins
// https://github.com/GreyGnome/EnableInterrupt/wiki/Usage#determine-the-pin-that-was-interrupted
#define EI_NOTINT0  
#define EI_NOTINT1  
#define EI_NOTINT2  
#define EI_NOTINT3  
#define EI_NOTINT6
//use to enable virtual external interrupt pins
#include <EnableInterrupt.h>

IMUturn::IMUturn(int motorCount1, int motorCount2, float diskcount, float diam, int pinAIN1, int pinAIN2, int pinPWMA, int pinBIN1, int pinBIN2, int pinPWMB, int pinSTBY): motorCtrl(pinAIN1, pinAIN2, pinPWMA, pinBIN1, pinBIN2, pinPWMB, pinSTBY)
{
  //constructor that inherits the motroCtrl library, adding IMU and controlled motion
  _motorCount1 = motorCount1;
  _motorCount2 = motorCount2;
  _diskcount = diskcount;
  _diam = diam;
  enableInterrupt(_motorCount1, ISR_count1, RISING);
  enableInterrupt(_motorCount2, ISR_count2, RISING);
}

void IMUturn::ISR_count1()
{
  _counter1++;
}

void IMUturn::ISR_count2()
{
  _counter2++;
}

void IMUturn::IMU_init(){
  //calls initialize in IMU.h to start and calibrate the IMU
  initialize();
}

float IMUturn::measureYAW(){
  //calls getYAW() from IMU.h to return current yaw angle estimation
  return getYAW();
}

float IMUturn::avg_YAW(){
  //compute the average yaw for better accuracy and robustness
  float init, yaw0;
  int i,j;
  j = 0;
  yaw0 = 0;
  
  for (i = 0; i < 5; i++) {
    init = getYAW();
    int Init = int(init*10);
    if (Init != 0) {
      yaw0 = init + yaw0;
      j++;
    }
   }
   return yaw0/j;
}

float IMUturn::theta_mapping(float theta_0, float Deg){
  //given current yaw theta_0 and the specified Deg of turning, map to target destination in the reference frame of the IMU
  float dest;
  dest = theta_0 + Deg;
  //if turned from postive half to negative half (in IMU reference frame)
  if (dest > 180.0 && dest < 360.0) {
    dest = -360.0 + dest;
    var = 1;
  }
  //if turned from negative half to positve half (In IMU reference frame)
  else if (dest > -360.0 && dest < -180.0) {
    dest = 360.0 + dest;
    var = 2;
  }
  //if turned within either half without crossing
  else if ((dest > -0.1 && dest < 180.0) || (dest > -180.0 && dest < 0.0)) {
    if (Deg > 0) {
      //turn right
      var = 3;
    } else {
      //turn left
      var = 4;
    }
  }
  //faulty case
  else {
    var = 5;
  }
  return dest;
}

void IMUturn::turn(float Deg, int vol, int fb[])
{
  //execute turning for certain Deg, given the specified speed vol, and return feedback
  float YAW_0, dest, init, YAW_1, YAW_diff;
  static float err=0;

  YAW_0 = avg_YAW();

  dest = theta_mapping(YAW_0, Deg);

  switch (var) {
    case 1:
      Serial.print("case 1");
      Serial.print("\n");
      Serial.print("YAW_0 ");
      Serial.print(YAW_0);
      Serial.print("\n");
      while (1) {
        YAW = avg_YAW();

        if (-0.1 < YAW && YAW < 180.0) {
        //first turn right to the crossing boundary
          motorCtrl::motorDrive(motor1, turnCW, vol);
          motorCtrl::motorDrive(motor2, turnCCW, vol);
        }
        else if (-180.0 < YAW && YAW < dest - err) {
        //then continues to turn right to destination
          motorCtrl::motorDrive(motor1, turnCW, vol);
          motorCtrl::motorDrive(motor2, turnCCW, vol);
        }
        else {
          motorCtrl::motorsStandby();
          YAW_1 = avg_YAW();
          break;
        }
      }
      break;
    case 2:
      Serial.print("case 2");
      Serial.print("\n");
      Serial.print("YAW_0 ");
      Serial.print(YAW_0);
      Serial.print("\n");
      while (1) {
        YAW = avg_YAW();
        //first turn left to the crossing boundary
        if (-180.0 < YAW && YAW < 0.0) {
          motorCtrl::motorDrive(motor1, turnCCW, vol);
          motorCtrl::motorDrive(motor2, turnCW, vol);
        }
        //then continues to turn left to destination
        else if (dest + err < YAW && YAW < 180.0) {
          motorCtrl::motorDrive(motor1, turnCCW, vol);
          motorCtrl::motorDrive(motor2, turnCW, vol);
        }
        else {
          motorCtrl::motorsStandby();
          YAW_1 = avg_YAW();
          break;
        }
      }
      break;
    case 3:
      Serial.print("case 3");
      Serial.print("\n");
      while (1) {
        YAW = avg_YAW();
        //turn right within either half till destination
        if (YAW < dest - err) {
          motorCtrl::motorDrive(motor1, turnCW, vol);
          motorCtrl::motorDrive(motor2, turnCCW, vol);
        } else {
          motorCtrl::motorsStandby();
	  YAW_1 = avg_YAW();
          break;      
        }
      }
      break;
    case 4:
      Serial.print("case 4");
      Serial.print("\n");
      while (1) {
        YAW = avg_YAW();
        //turn left within either half till destination
        if (YAW > dest + err) {
          motorCtrl::motorDrive(motor1, turnCCW, vol);
          motorCtrl::motorDrive(motor2, turnCW, vol);
        } else {
          motorCtrl::motorsStandby();
	  YAW_1 = avg_YAW();
          break;
          
        }
      }
      break;
    case 5:
      Serial.print("case 5");
      Serial.print("\n");
      break;
  }
  YAW_diff = abs(YAW_1-YAW_0);
  Serial.print("Went from ");
  Serial.print(YAW_0);
  Serial.print(" to ");
  Serial.print(YAW_1);
  Serial.print(" for ");
  Serial.println(YAW_diff);
  //send yaw measurement after turning as feedback
  fb[0] = YAW_1;
}

int IMUturn::CMtoCounts(float cm)
{
  //converts distance command in cm to number of counts of the disk slots on the wheels
  int results;
  float circumference = (_diam * 3.14) / 10;
  float cm_count = circumference / _diskcount;
  float counts = cm / cm_count;
  results = int(counts);
  Serial.print("need ticks: ");
  Serial.println(results);
  return results;
}

void IMUturn::goForward(int counts, int speed, int fb[])
{
  //drive forward for certain counts at specified speed, and returns feedback
  _counter1 = 0;
  _counter2 = 0;

  float yaw0 = avg_YAW();
  delay(50);
  while (counts > _counter1 && counts > _counter2) {
    if (counts > _counter2) {
      motorCtrl::motorDrive(motor2, turnCW, speed);
    } else {
      motorCtrl::motorStop(motor2);

    }
    if (counts > _counter1) {
      motorCtrl::motorDrive(motor1, turnCW, speed);
    } else {
      motorCtrl::motorStop(motor1);
    }
  }

  Serial.print("counter1: ");
  Serial.print(_counter1);
  Serial.print("counter2: ");
  Serial.print(_counter2);

  motorCtrl::motorsStandby();
  delay(50);
  float yaw1 = avg_YAW();

  //returns the interpolated average yaw before and after executtion to account for drifting
  fb[0] = (yaw0+yaw1)/2;
  Serial.print("average yaw: ");
  Serial.print((yaw0+yaw1)/2);
  _counter1 = 0;
  _counter2 = 0;
}

void IMUturn::goBackward(int counts, int speed, int fb[])
{
  //drive backward for certain counts at specified speed, and returns feedback
  _counter1 = 0;
  _counter2 = 0;

  float yaw0 = avg_YAW();
  delay(50);
  while (counts > _counter1 && counts > _counter2) {
    if (counts > _counter2) {
      motorCtrl::motorDrive(motor2, turnCCW, speed);
    } else {
      motorCtrl::motorStop(motor2);
    }
    if (counts > _counter1) {
      motorCtrl::motorDrive(motor1, turnCCW, speed);
    } else {
      motorCtrl::motorStop(motor1);
    }
  }

  Serial.print("counter1: ");
  Serial.print(_counter1);
  Serial.print("counter2: ");
  Serial.print(_counter2);
  
  motorCtrl::motorsStandby();
  delay(50);

  float yaw1 = avg_YAW();
  //returns the interpolated average yaw before and after executtion to account for drifting
  fb[0] = (yaw0+yaw1)/2;
  Serial.print("average yaw: ");
  Serial.print((yaw0+yaw1)/2);
  _counter1 = 0;
  _counter2 = 0;
}

