#include "Arduino.h"
#include "motorCtrl.h"

motorCtrl::motorCtrl(int pinAIN1, int pinAIN2, int pinPWMA, int pinBIN1, int pinBIN2, int pinPWMB, int pinSTBY)
{
  //constructor that sets all the pins of the motor driver
  pinMode(pinPWMA, OUTPUT);
  pinMode(pinAIN1, OUTPUT);
  pinMode(pinAIN2, OUTPUT);
  pinMode(pinPWMB, OUTPUT);
  pinMode(pinBIN1, OUTPUT);
  pinMode(pinBIN2, OUTPUT);
  pinMode(pinSTBY, OUTPUT);

  _pinPWMA = pinPWMA;
  _pinAIN1 = pinAIN1;
  _pinAIN2 = pinAIN2;
  _pinPWMB = pinPWMB;
  _pinBIN1 = pinBIN1;
  _pinBIN2 = pinBIN2;
  _pinSTBY = pinSTBY;

}

void motorCtrl::motorDrive(boolean motorNumber, boolean motorDirection, int motorSpeed)
{
   
 /*
  This Drives a specified motor, in a specific direction, at a specified speed:
 */

  boolean pinIn1;  //Relates to AIN1 or BIN1
 
  //Specify the Direction to turn the motor
  if (motorDirection == 0) //clockwise->forward
    pinIn1 = HIGH;
  else
    pinIn1 = LOW;

//Select the motor to turn, and set the direction and the speed
  if(motorNumber == 0)
  {
    digitalWrite(_pinAIN1, pinIn1);
    digitalWrite(_pinAIN2, !pinIn1);  //This is the opposite of the AIN1
    analogWrite(_pinPWMA, motorSpeed);
  }
  else
  {
    digitalWrite(_pinBIN1, pinIn1);
    digitalWrite(_pinBIN2, !pinIn1);  //This is the opposite of the BIN1
    analogWrite(_pinPWMB, motorSpeed);
  }
   
//disabled STBY- pull it HIGH
  digitalWrite(_pinSTBY, HIGH);
}

void motorCtrl::motorStop(boolean motorNumber)
{
    /*
  This stops the specified motor by setting both IN pins to LOW
  */
  if (motorNumber == 0) 
  {
    digitalWrite(_pinAIN1, LOW);
    digitalWrite(_pinAIN2, LOW);
  }
  else
  {
    digitalWrite(_pinBIN1, LOW);
    digitalWrite(_pinBIN2, LOW);
  } 
}

void motorCtrl::motorsStandby()
{
  /*
  This puts the motors into Standby Mode
  */
  digitalWrite(_pinSTBY, LOW);
}

void motorCtrl::motorBrake(boolean motorNumber)
{
  /*
  This "Short Brake"s the specified motor, by setting speed to zero
  */

  if (motorNumber == 0)
    analogWrite(_pinPWMA, 0);
  else
    analogWrite(_pinPWMB, 0);
}
