#ifndef MOTORCTRL_H
#define MOTORCTRL_H

#include "Arduino.h"

class motorCtrl
{
  public:
    motorCtrl(int pinAIN1, int pinAIN2, int pinPWMA,
    int pinBIN1, int pinBIN2, int pinPWMB, int pinSTBY);
    virtual void motorDrive(boolean motorNumber, boolean motorDirection, int motorSpeed);
    virtual void motorStop(boolean motorNumber);
    virtual void motorsStandby();
    virtual void motorBrake(boolean motorNumber);

  private:
    //motor driver pins
    int _pinAIN1;//connect to A01
    int _pinAIN2;//connect to A02
    int _pinPWMA;//PWM controll for A
    int _pinBIN1;//connect to B01
    int _pinBIN2;//connect to B02
    int _pinPWMB;//PWM controll for B
    int _pinSTBY;//standby pin
  
};


#endif